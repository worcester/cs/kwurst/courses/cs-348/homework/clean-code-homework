# Clean Code Homework

**For the actual homework assignment, go to [CleanCodeHomework.md](./CleanCodeHomework.md).**

This assignment has a document with instructions for the assignment.

1. Fork this project to your `https://gitlab.com/worcester/cs/cs-348-01-02-e1-fall-2024/students/your-name` subgroup.
2. Open it with Gitpod.

Copyright © 2024 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
