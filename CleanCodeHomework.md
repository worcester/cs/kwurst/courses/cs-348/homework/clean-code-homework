# Copyright and Licensing Homework

## Objectives

Be able to apply principles and practices from Clean Code to improve code.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a 
*Meets Specification* fashion.**

### Set Up Repository for Your Code

1. Select a program that you wrote (for a prior class or outside project) to review and revise. Be sure to select code that you are confident you can significantly improve by applying principles and practices from Clean Code.
2. Copy your code into your fork of this homework repository.
3. Commit the original code with the commit message `Original code.`
4. Push your repository to GitLab.

### Refactor Your Code Based on Clean Code Principles

1. Apply principles and practices from Clean Code to the code in your repository. You must apply **at least two** principles/practices **from each of the activities CleanCode1-CleanCode5**.
    1. You may also use outside sources/summaries of Clean Code that you find on the Internet. If you do so, you must include references to each source you used in your reflection document.
2. In the file in your repository called `Reflection.md` explain the changes you have made. (Between 250 and 500 words.)
    1. Include your reflection in the  `## Base` section.
    2. For each CleanCode1-CleanCode5 activity, describe at least two changes you made and the practice/principle that you applied to make that change. **Include line numbers from the `Original code.` commit and the commit where the change appears for the first time.**
    3. Must be organized, clear, and name specific practices/principles from each CleanCode1-CleanCode5 activity (be sure you identify the activity).
    4. Include references to outside Clean Code sources/summaries.
3. Be sure you have committed your changes. You may make more than one commit to your repository. You **do not** have to save all your changes in a single commit.

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards
base course grades of C or higher.**

* See the the Course Grade Determination table in the syllabus to see how many
Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received
an Acceptable grade on.
* You may not need to submit one for this assignment.

### More Refactoring of Your Code Based on Clean Code Principles

**Beyond the changes you made for the Base Assignment, you must:**

1. Make **at least 8 more changes** to your code based on the  principles/practices **from any mix** of the **activities CleanCode1-CleanCode5 or outside sources**.
    1. If you cannot find 8 more changes to make, you may use another piece of code instead.
2. Add a new section to your reflection report that explains the changes you have made. (Between 250 and 500 **more** words.)
    1. Include your reflections in the  `## Intermediate` section.
    2. For each change, describe the change you made and the practice/principle that you applied to make that change. **Include line numbers from the `Original code.` commit and the commit where the change appears for the first time.**
    3. **Discuss how your thinking about how to write/structure your code has changed after reading about Clean Code.**
    4. Must be organized, clear, and name specific practices/principles (be sure you identify the the source).
3. Be sure you have committed your changes. You may make more than one commit to your repository. You **do not** have to save all your changes in a single commit.

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards
base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many
Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received
an Acceptable grade on.
* You may not need to submit one for this assignment.

### Refactor Your Code to the Fullest Extent Possible

1. Refactor your code to the fullest extent possible. In other words, keep applying Clean Code principles to your code until you don't see any other reasonable places to refactor. I want to see your code in a condition that Uncle Bob would approve of.
    1. If your original code from the base or intermediate assignments has no more changes to be made, you may select another piece of code to refactor.
2. Add a new section to your reflection report that explains the changes you have made. (Between 250 and 500 **more** words.)
    1. Include your reflections in the `## Advanced` section.
    2. For each change, describe the change you made and the practice/principle that you applied to make that change. **Include line numbers from the `Original code.` commit and the commit where the change appears for the first time.**
    3. **Explain why you feel that there are no other reasonable places to refactor.**
    4. **Discuss how you feel about your original code, and how you feel about your fully refactored code.**
3. Be sure you have committed your changes. You may make more than one commit to your repository. You **do not** have to save all your changes in a single commit.

#### Deliverables

* Add and commit your changes.
* Push your changes.

&copy; 2024 Karl R. Wurst and Shruti Nagpal.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

